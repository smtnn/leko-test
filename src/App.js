import React, {useEffect}  from 'react';
import CharactersList from './components/CharactersList/'
import Header from './components/Header/'
import CharacterEditor from './components/CharacterEditor/'
import './App.css';

function App() {
  return (
    <div className='app'>
      <Header />
      <CharactersList />
      <CharacterEditor />
    </div>
  )
}

export default App;
