import axios from 'axios'
const MOCK_ENDPOINT = {
    200: 'https://www.mocky.io/v2/5e92a9b93000007600156652',
    201: 'https://www.mocky.io/v2/5e92abaf300000550015665d'
}

export const OPEN_CHARACTER_EDITOR = 'OPEN_CHARACTER_EDITOR'
export const CLOSE_CHARACTER_EDITOR = 'CLOSE_CHARACTER_EDITOR'
export const HANDLE_CHANGE_CHARACTER_DATA = 'HANDLE_CHANGE_CHARACTER_DATA'
export const HANDLE_CHANGE_FILTER_QUERY = 'HANDLE_CHANGE_FILTER_QUERY'
export const HANDLE_CHANGE_SORT = 'HANDLE_CHANGE_SORT'
export const HANDLE_CHANGE_LIST_VIEW = 'HANDLE_CHANGE_LIST_VIEW'

export const openEditor = data => action(OPEN_CHARACTER_EDITOR, {data})
export const closeEditor = () => action(CLOSE_CHARACTER_EDITOR)
export const handleChangeCharacterData = (fieldName, value) => action(HANDLE_CHANGE_CHARACTER_DATA, {fieldName, value})
export const handleChangeFilterQuery = query => action(HANDLE_CHANGE_FILTER_QUERY, {query})
export const handleChangeSort = sortBy => action(HANDLE_CHANGE_SORT, {sortBy})
export const handleChangeListView = layout => action(HANDLE_CHANGE_LIST_VIEW, {layout})

export const REQUEST = 'REQUEST'
export const SUCCESS = 'SUCCESS'
export const FAILURE = 'FAILURE'
export const GET_CHARACTERS = createRequestTypes('GET_CHARACTERS')
export const ADD_CHARACTER = createRequestTypes('ADD_CHARACTER')
export const UPDATE_CHARACTER = createRequestTypes('UPDATE_CHARACTER')
export const REMOVE_CHARACTER = createRequestTypes('REMOVE_CHARACTER')

export const getCharacters = () => dispatch => {
    dispatch(action(GET_CHARACTERS[REQUEST]))
    return axios.get('/data.json')
        .then(
            res => {
                if (res.status === 200)
                    dispatch(action(GET_CHARACTERS[SUCCESS], {data: res.data || []}))
                else
                    dispatch(action(GET_CHARACTERS[FAILURE]))
            },
            error => {
                dispatch(action(GET_CHARACTERS[FAILURE]))
                console.log(error)
            }
        )
}

export const updateCharacter = (id, update) => dispatch => {
    dispatch(action(UPDATE_CHARACTER[REQUEST]))
    return axios.put(MOCK_ENDPOINT[200], {id, update})
        .then(
            res => {
                if (res.status === 200)
                    dispatch(action(UPDATE_CHARACTER[SUCCESS], {id, update}))
                else
                    dispatch(action(UPDATE_CHARACTER[FAILURE]))
            },
            error => {
                dispatch(action(UPDATE_CHARACTER[FAILURE]))
                console.log(error)
            }
        )
}

export const addCharacter = character => dispatch => {
    dispatch(action(ADD_CHARACTER[REQUEST]))
    return axios.put(MOCK_ENDPOINT[201])
        .then(
            res => {
                if (res.status === 201)
                    dispatch(action(ADD_CHARACTER[SUCCESS], {character}))
                else
                    dispatch(action(ADD_CHARACTER[FAILURE]))
            },
            error => {
                dispatch(action(ADD_CHARACTER[FAILURE]))
                console.log(error)
            }
        )
}

export const removeCharacter = id => dispatch => {
    dispatch(action(REMOVE_CHARACTER[REQUEST]))
    return fetch(MOCK_ENDPOINT[200])
        .then(
            res => {
                if (res.status === 200)
                    dispatch(action(REMOVE_CHARACTER[SUCCESS], {id}))
                else
                    dispatch(action(REMOVE_CHARACTER[FAILURE]))
            },
            error => {
                dispatch(action(REMOVE_CHARACTER[FAILURE]))
                console.log(error)
            }
        )
}

function createRequestTypes(base) {
    return [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
        acc[type] = `${base}_${type}`
        return acc
    }, {})
}
function action(type, payload = {}) {
    return {type, ...payload}
}