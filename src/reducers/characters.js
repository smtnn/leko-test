import {
    GET_CHARACTERS,
    ADD_CHARACTER,
    REMOVE_CHARACTER,
    UPDATE_CHARACTER,
    OPEN_CHARACTER_EDITOR,
    CLOSE_CHARACTER_EDITOR,
    HANDLE_CHANGE_CHARACTER_DATA,
    HANDLE_CHANGE_FILTER_QUERY,
    HANDLE_CHANGE_SORT,
    HANDLE_CHANGE_LIST_VIEW,
    REQUEST,
    SUCCESS,
    FAILURE
} from '../actions/characters'
const shortid = require('shortid')

const initialState = {
    loading: false,
    query: '',
    data: [],
    sortBy: 'name',
    editor: {
        active: false,
        formData: {},
        error: null
    },
    layout: 'table',
    columns: [
        {
            title: 'Name',
            dataIndex: 'name',
            width: '15%'
        },
        {
            title: 'Role',
            dataIndex: 'role',
            width: '20%'
        },
        {
            title: 'Execution',
            dataIndex: 'execution',
            width: '40%'
        },
        {
            title: 'Killed by',
            dataIndex: 'killedBy',
            width: '15%'
        },
        {
            title: 'Weapon',
            dataIndex: 'weapon',
            width: '8%'
        }
    ]
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_CHARACTERS[SUCCESS]: {
            const {data = []} = action
            return {
                ...state,
                data,
                loading: false
            }
        }
        case ADD_CHARACTER[SUCCESS]: {
            const {character = {}} = action
            character['id'] = shortid()
            return {
                ...state,
                data: [...state.data, character],
                loading: false
            }
        }
        case REMOVE_CHARACTER[SUCCESS]: {
            const {id} = action
            return {
                ...state,
                data: state.data.filter(i => i && i.id !== id),
                loading: false
            }
        }
        case UPDATE_CHARACTER[SUCCESS]: {
            const {id, update} = action
            const data = state.data ? [...state.data] : []
            const targetIndex = data.map(i => i && i.id).indexOf(id)

            if (update && targetIndex !== -1)
                data[targetIndex] = update

            return {
                ...state,
                data,
                loading: false
            }
        }
        case GET_CHARACTERS[REQUEST]:
        case ADD_CHARACTER[REQUEST]:
        case REMOVE_CHARACTER[REQUEST]:
        case UPDATE_CHARACTER[REQUEST]:
            return {
                ...state,
                loading: true,
                editor: initialState.editor
            };
        case GET_CHARACTERS[FAILURE]:
        case ADD_CHARACTER[FAILURE]:
        case REMOVE_CHARACTER[FAILURE]:
        case UPDATE_CHARACTER[FAILURE]:
            return {
                ...state,
                loading: false
            };
        case HANDLE_CHANGE_LIST_VIEW: {
            const {layout} = action
            return {
                ...state,
                layout
            }
        }
        case OPEN_CHARACTER_EDITOR: {
            const {data} = action
            return {
                ...state,
                editor: {
                    active: true,
                    formData: data || {}
                }
            }
        }
        case CLOSE_CHARACTER_EDITOR: {
            return {
                ...state,
                editor: initialState.editor
            }
        }
        case HANDLE_CHANGE_CHARACTER_DATA: {
            const {fieldName, value} = action
            return {
                ...state,
                editor: {
                    ...state.editor,
                    formData: {
                        ...state.editor.formData,
                        [fieldName]: value
                    }
                }
            }
        }
        case HANDLE_CHANGE_SORT: {
            const {sortBy} = action
            return {
                ...state,
                sortBy
            }
        }
        case HANDLE_CHANGE_FILTER_QUERY: {
            const {query} = action
            return {
                ...state,
                query
            }
        }
        default: return state
    }
}