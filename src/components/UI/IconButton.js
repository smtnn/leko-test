import {Button, Tooltip} from "antd";
import React from "react";

export default props => {
    const {icon, title = '', onClick} = props
    return <Tooltip title={title}>
        <Button
            style={{width:32, padding:0}}
            onClick={onClick}
        >
            <i className={icon}></i>
        </Button>
    </Tooltip>
}