import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    closeEditor,
    handleChangeCharacterData,
    addCharacter,
    updateCharacter
} from '../../actions/characters'
import { Modal, Input } from 'antd';
import {isEmpty} from 'lodash'

export default props => {
    const dispatch = useDispatch();
    const {editor = {}, columns} = useSelector(state => state.characters)
    const {
        active,
        formData = {}
    } = editor || {}
    const id = formData && formData.id
    const isValidCharacter = !isEmpty(formData) && (columns.filter(i => !formData[i.dataIndex]).length === 0)

    return (
        <Modal
            title={`${id ? 'Edit' : 'Add'} character`}
            visible={active}
            onOk={() => dispatch(id ? updateCharacter(id, formData) : addCharacter(formData))}
            onCancel={() => dispatch(closeEditor())}
            okButtonProps={{ disabled: !isValidCharacter }}
        >
            {
                columns.map(i => {
                    const {dataIndex, title} = i
                    return <Input
                        key={'editor_'+dataIndex}
                        value={formData && formData[dataIndex]}
                        placeholder={title}
                        onChange={e => dispatch(handleChangeCharacterData(dataIndex, e.target.value))}
                        style={{margin: '10px 0'}}
                        size='large'
                    />
                })
            }
        </Modal>
    );
}
