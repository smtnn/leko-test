import React from 'react';
import {Table} from "antd";
import {useDispatch} from "react-redux";
import {removeCharacter, openEditor} from '../../actions/characters'

import IconButton from '../UI/IconButton'

export default props => {
    const dispatch = useDispatch()
    const {items, columns, loading} = props
    return (
        <Table
            rowKey={item => item.id}
            dataSource={items}
            loading={loading}
            onRow={item => ({onClick: event => dispatch(openEditor(item))})}
            columns={[
                ...columns,
                {
                    title: '',
                    render: (text, item) => {
                        return <IconButton
                            title='Delete'
                            icon='fas fa-trash'
                            onClick={e => {
                                e.stopPropagation()
                                dispatch(removeCharacter(item.id))
                            }}
                        />
                    },
                    width: '2%'
                }
            ]}
        />
    );
}
