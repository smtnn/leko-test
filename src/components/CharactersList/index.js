import React, {useEffect}  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {getCharacters} from "../../actions/characters";
import Grid from './Grid'
import Table from './Table'
import {orderBy, isEmpty, forEach} from 'lodash'

export default () => {
    const dispatch = useDispatch();
    const {
        data,
        layout,
        sortBy,
        query,
        loading,
        columns
    } = useSelector(state => state.characters)
    const ListComponent = layout === 'grid' ? Grid : Table
    let items = data || []

    if (query)
        items = items.filter(character => {
            let match = false;
            if (!isEmpty(character))
                forEach(
                    Object.keys(character),
                        attr => {
                        const value = character && character[attr]
                        if (attr !== 'id' && value && typeof value === 'string' && value.toLowerCase().indexOf(query.toLowerCase()) !== -1)
                            match = true
                        }
                )
            return match
        })

    if (sortBy)
        items = orderBy(items, [item => item[sortBy].toLowerCase()], ['asc'])

    useEffect(() => {
        dispatch(getCharacters())
    }, [dispatch])

    return <ListComponent
        items={items}
        loading={loading}
        columns={columns}
    />
}
