import React from 'react';
import {Card} from 'antd'
import {useDispatch} from "react-redux";
import {removeCharacter, openEditor} from '../../actions/characters'
import IconButton from '../UI/IconButton'

const {Meta} = Card
const containerStyles = {display:'flex', flexWrap: 'wrap', padding: 10}
const cardWrapperStyles = {
    minWidth: 250,
    maxWidth: 500,
    flexBasis: '20%',
    flexGrow: 0,
    flexShrink: 0,
    padding: 10
}
const cardStyles = {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
}

export default props => {
    const dispatch = useDispatch()
    const {items, columns, loading} = props
    return (
        <div style={containerStyles}>
            {items && items.map(i => {
                const {id, name} = i || {}
                return (
                    <div key={id} style={cardWrapperStyles}>
                        <Card
                            style={cardStyles}
                            actions={[
                                '',
                                <IconButton
                                    title='Delete'
                                    icon='fas fa-trash'
                                    onClick={e => dispatch(removeCharacter(id))}
                                />,
                                <IconButton
                                    title='Edit character'
                                    icon='fas fa-user-edit'
                                    onClick={event => dispatch(openEditor(i))}
                                />
                            ]}
                        >
                            <Meta
                                title={name}
                                description={
                                    <>
                                        {
                                            columns.map(col => {
                                                const {dataIndex, title} = col
                                                return dataIndex !== 'name' ? <div key={id + ' ' + dataIndex}>
                                                    <strong>{title+': '}</strong>
                                                    <span>{i && i[dataIndex]}</span>
                                                </div> : ''
                                            })
                                        }
                                    </>
                                }
                            />
                        </Card>
                    </div>
                )}
            )}
        </div>
    )
}