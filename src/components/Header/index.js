import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    PageHeader,
    Input,
    Select,
    Tooltip
} from 'antd'
import IconButton from '../UI/IconButton'
import {
    handleChangeFilterQuery,
    handleChangeListView,
    handleChangeSort,
    openEditor
} from "../../actions/characters";

const { Option } = Select;
const headerStyle = {
    position: 'fixed',
    zIndex: 1,
    width: '100%',
    top: 0,
    background: '#f0f0f0',
    whiteSpace: 'nowrap',
    maxHeight: 64
}

export default () => {
    const dispatch = useDispatch();
    const {query, sortBy, layout, columns} = useSelector(state => state.characters)
    return (
        <PageHeader
            title='GoT characters deaths stats'
            style={headerStyle}
            extra={[
                <IconButton
                    key='toggle_view'
                    title={layout === 'table' ? 'Grid view' : 'Table view'}
                    onClick={e => dispatch(handleChangeListView(layout === 'table' ? 'grid' : 'table'))}
                    icon={`fas ${layout === 'table' ? 'fa-th' : 'fa-th-list'}`}
                />,
                <IconButton
                    key='add_character'
                    title="Add character"
                    onClick={e => dispatch(openEditor())}
                    icon='fas fa-user-plus'
                />,
                <Tooltip
                    key='sort_by'
                    title="Sort by"
                    placement='bottom'
                >
                    <Select
                        placeholder='Sort by'
                        value={sortBy}
                        onChange={value => dispatch(handleChangeSort(value))}
                        style={{minWidth: 120}}
                    >
                        {
                            columns.map(col => {
                                const {title, dataIndex} = col
                                return <Option value={dataIndex} key={'sort_option_'+dataIndex}>{title}</Option>
                            })
                        }
                    </Select>
                </Tooltip>,
                <Input
                    key='search'
                    type="text"
                    placeholder='Search'
                    value={query||''}
                    prefix={<i style={{opacity: 0.8}} className='fas fa-search'></i>}
                    style={{width: 180}}
                    onChange={e => dispatch(handleChangeFilterQuery(e.target.value))}
                />
            ]}
        />
        )

};
