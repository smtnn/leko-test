## GoT characters deaths stats app

[DEMO](https://tender-northcutt-b54e62.netlify.com/)

1. To pull npm dependencies run `npm i` in the root directory.
2. Use command `npm start` to run dev server or `npm run build` to create static bundle.

By default development server starts at [http://localhost:3000](http://localhost:3000)